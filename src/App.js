import React from 'react';
import logo from './logo.svg';
import './App.css';
import ListItems from './ListItems'
import { library } from '@fortawesome/fontawesome-svg-core'
import { faTrash } from '@fortawesome/free-solid-svg-icons'
import { faEdit } from '@fortawesome/free-solid-svg-icons'

library.add(faTrash)
library.add(faEdit)



class App extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      items: [],
      currentItem: {
        text: '',
        key: '',
        status: false
      },
      search: ''
    }
  }


  addItem = (e) => {
    e.preventDefault();
    const newItem = this.state.currentItem;
    if (newItem.text !== '') {
      const items = [...this.state.items, newItem];
      this.setState({
        items: items,
        currentItem: {
          text: '',
          key: ''
        }
      })
    }
  }


  handleInput = (e) => {
    this.setState({
      currentItem: {
        text: e.target.value,
        key: Date.now()
      }
    })
  }


  deleteItem = (key) => {
    const filteredItems = this.state.items.filter(item =>
      item.key !== key);
    this.setState({
      items: filteredItems
    })
  }


  setUpdate = (text, key) => {
    console.log("items:" + this.state.items);
    const items = this.state.items;
    items.map(item => {
      if (item.key === key) {
        console.log(item.key + "    " + key)
        item.text = text;
      }
    })
    this.setState({
      items: items
    })
  }

  /*completetask = (status,key) => {
    const items = this.state.items;
    items.map(item => {
      if (item.key === key) {
        item.status = true;
        console.log(status)
      }
    })
    this.setState({
      items: items
    })
  }*/


  handleChange = (e) => {
    this.setState({
      search: e.target.value
    })

    const filteredItems = this.state.items.filter(item =>
      item.text.startsWith(this.state.search));
    this.setState({ items: filteredItems })

  }

  render() {
    return (
      <div className="App">
        <div>
          <form id="to-do-form" onSubmit={this.addItem}>
            <input type="text" placeholder="Entrer la tache" value={this.state.currentItem.text} onChange={this.handleInput} />
            <button type="submit">Ajouter</button>
          </form>

          <form id="form-recherch">
            <input type="text" placeholder="rechercher une tache" value={this.state.search} onChange={this.handleChange} />
          </form>



          <p>{this.state.items.text}</p>

          <ListItems items={this.state.items} deleteItem={this.deleteItem} setUpdate={this.setUpdate} />

        </div>
      </div>
    );
  }
}


export default App;
